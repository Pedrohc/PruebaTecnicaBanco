# PruebaTecnicaBanco


# Prueba Técnica: Operaciones Bancarias

Esta es una prueba técnica que simula las operaciones bancarias como el registro de un cliente, así como la creación de su cuenta bancaria y registro de transacciones.

## Descripción del Reto

El reto consistía en:

1. **Crear un cliente**:
    - Crear una entidad que heredara por anotaciones de una clase `Persona` para crear la entidad `Cliente`.

2. **Microservicio de Cliente**:
    - Este microservicio debía comunicarse con dos microservicios adicionales:
        - **Microservicio de Cuenta Bancaria**: Este servicio almacenaría el tipo de cuenta y el saldo en ella.
        - **Microservicio de Transacciones**: Este servicio debía registrar cada movimiento realizado por el cliente en su cuenta y devolver el nuevo monto al microservicio de cuenta bancaria.

3. **Patrón de Diseño Utilizado**:
    - Para la solución se utilizó un patrón de diseño de comportamiento, `Chain of Responsibility`.
    - Al llegar al endpoint de cobro, debe poder diferenciar el tipo de cuenta y hacer el cobro según la lógica de la misma (crédito o débito).
    - Para validación de los datos de entrada del cliente se ocupo el patrón, `Validator Pattern`.

4. **Comunicación entre Servicios**:
    - Para la comunicación entre servicios se utilizó `OpenFeign` y un balanceador de carga `Eureka`, el cual permitía tener más de un microservicio registrado con el mismo nombre, proporcionando redundancia en caso de que un servicio fallara.

## Detalles Técnicos

- **Cliente**: Entidad que hereda de `Persona`.
- **Cuenta Bancaria**: Servicio encargado de almacenar tipo de cuenta y saldo.
- **Transacciones**: Servicio encargado de registrar movimientos y actualizar el saldo en el microservicio de cuenta bancaria.
- **Patrón de Diseño**: `Chain of Responsibility` utilizado para gestionar la lógica de cobro según el tipo de cuenta (crédito o débito).
- **Comunicación entre Servicios**: Utilización de `OpenFeign` y `Eureka` para balanceo de carga y redundancia.

---