package tel.neoris.exceptions;


import org.springframework.http.HttpStatus;


public class ApiException {
    private final String message;
    // Agrega el valor del campo
    private final HttpStatus httpStatus;

    public ApiException(String message, HttpStatus httpStatus) {
        this.message = message;

        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }


    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
