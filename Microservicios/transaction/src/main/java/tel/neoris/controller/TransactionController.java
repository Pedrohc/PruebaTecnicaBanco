package tel.neoris.controller;


import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tel.neoris.abstracFactory.TransactionDtoFactory;
import tel.neoris.dto.TransactionDto;
import tel.neoris.exceptions.ApiRequestException;
import tel.neoris.model.Transaction;
import tel.neoris.service.TransactionServiceImplementation;

import java.util.List;

@RestController
@RequestMapping
@AllArgsConstructor
public class TransactionController {

    private TransactionServiceImplementation transactionServiceImplementation;
    private TransactionDtoFactory dtoFactory;


    @GetMapping(value = "/selectTransactions")
    public ResponseEntity<?> findAllTransaction() {

        return ResponseEntity.ok(transactionServiceImplementation.select());
    }

    @GetMapping(value = "/findOneTransaction/{id}")
    public ResponseEntity<?> findOneTransaction(@PathVariable Long id) {

        return ResponseEntity.ok(transactionServiceImplementation.findOne(id)
                .orElseThrow(() -> new ApiRequestException("no se encontro la transaccion")));

    }

    @PostMapping(value = "/insertTransaction")
    public ResponseEntity<?> insert(@Valid @RequestBody Transaction transaction) {

        return ResponseEntity.ok().body(transactionServiceImplementation.insert(transaction));

    }

    @PutMapping(value = "/updateTransaction")
    public ResponseEntity<?> update(@Valid @RequestBody Transaction transaction) {
        return ResponseEntity.ok().body(transactionServiceImplementation.insert(transaction));
    }


    @DeleteMapping(value = "/deleteTransaction/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable Long id) {
        if (transactionServiceImplementation.findOne(id).isPresent()) {
            transactionServiceImplementation.deletById(id);
            return ResponseEntity.ok().body("Eliminaste la transaccion con  id: " + id);
        }
        throw new ApiRequestException("no se encontro la transaccion a eliminar");

    }

    @GetMapping("/reportByDate")
    public ResponseEntity<List<?>> buscarTransacciones(
            @RequestParam(name = "accountId") Long accountId,
            @RequestParam(name = "fechaInicio")  String fechaInicio,
            @RequestParam(name = "fechaFin")  String fechaFin) {


        List<TransactionDto> transacciones = transactionServiceImplementation
                .report(accountId, fechaInicio, fechaFin)
                .stream()
                .map(dtoFactory::createDto)
                .toList();

        return ResponseEntity.ok(transacciones);


    }


}
