package tel.neoris.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tel.neoris.model.Transaction;

import java.sql.Date;
import java.util.List;


@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {

    @Query("SELECT t FROM Transaction t WHERE t.idAccountReference = ?1 AND t.date BETWEEN ?2 AND ?3")
    List<Transaction> findByAccountRefAndDateBetween(Long accountRef,Date fechaInicio,Date fechaFin);
    List<Transaction> findByidAccountReferenceAndDateBetween(Long accountRef, String begin, String end);



}
