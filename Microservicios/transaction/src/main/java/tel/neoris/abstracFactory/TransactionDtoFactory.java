package tel.neoris.abstracFactory;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tel.neoris.dto.TransactionDto;
import tel.neoris.model.Transaction;

@Component
@Qualifier("transactionDtoFactory")
public class TransactionDtoFactory implements DtoFactory {
    @Override
    public TransactionDto createDto(Object entity) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, TransactionDto.class);
    }

    @Override
    public Transaction createEntity(Object entity) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, Transaction.class);
    }
}
