package tel.neoris.abstracFactory;



import tel.neoris.dto.TransactionDto;
import tel.neoris.model.Transaction;

public interface DtoFactory {
    TransactionDto createDto(Object entity);
    Transaction createEntity(Object entity);
}
