package tel.neoris.enums;

public enum TypeAccount {
    NOMINA,
    AHORRO,
    RETIRO,
    INVERSION,
    CREDITO
}
