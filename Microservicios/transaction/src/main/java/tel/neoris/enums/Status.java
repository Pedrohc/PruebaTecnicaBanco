package tel.neoris.enums;


import lombok.Getter;

@Getter
public enum Status {
    ACTIVO,
    INACTIVO,
    DEUDOR,
    BURO
/*
    ACTIVO("activo"),
    INACTIVO("inactivo"),
    DEUDOR("deudor"),
    BURO("buro");

    private final String formatted;

    Status(String formatted) {
        this.formatted = formatted;
    }
/*
    private static Map<String, Status> FORMAT_MAP = Stream
            .of(Status.values())
            .collect(Collectors.toMap(s -> s.formatted, Function.identity()));
    @JsonCreator // This is the factory method and must be static
    public static Status fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElseThrow(() -> new HttpMessageNotReadableException(string));
    }


 */
}
