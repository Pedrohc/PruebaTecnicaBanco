package tel.neoris.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tel.neoris.model.Transaction;
import tel.neoris.repository.TransactionRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor

public class TransactionServiceImplementation implements TransactionServiceInterface {

    private TransactionRepository transactionRepository;
    @Override
    public List<?> select() {
        return transactionRepository.findAll();
    }

    @Override
    public String deletById(Long id) {
        String erase = transactionRepository.findById(id).toString();
        transactionRepository.deleteById(id);
        return "Borraste al cliente: " + erase + " pasa por tu renuncia a RH";
    }

    @Override
    public Optional<Transaction> findOne(Long id) {

        return transactionRepository.findById(id);
    }

    @Override
    public Transaction insert(Transaction transaction) {

        return transactionRepository.save(transaction);
    }
/*
    @Override
    public List<Transaction> report(Long accountId, Date begin, Date end) {
        return transactionRepository.findByAccountRefAndDateBetween(accountId,begin,end);
    }


 */
    @Override
    public List<Transaction> report(Long accountId, String begin, String end) {
        return transactionRepository.findByidAccountReferenceAndDateBetween(accountId,begin,end);
    }


}
