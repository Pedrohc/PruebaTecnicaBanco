package tel.neoris.service;



import tel.neoris.model.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionServiceInterface {
    List<?> select();
    String deletById(Long id);
    Optional<Transaction> findOne(Long id);
    Transaction  insert(Transaction transaction);

    List<?> report(Long clientID, String begin, String end);


}
