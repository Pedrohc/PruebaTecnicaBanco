package tel.neoris.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tel.neoris.enums.TypeTransaction;


import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transaction", nullable = false)
    private Long idTransaction;
    @Builder.Default
    @Column(
            name = "created_at",
            nullable = false
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss" // Ajusta el patrón para incluir fecha y hora
    )
    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());

    @Builder.Default
    @Column(
            name = "date",
            nullable = false
    )

    private String date = String.valueOf(LocalDate.now());

    @Column(
            name = "type_transaction",
            nullable = false
    )
    @Enumerated(EnumType.STRING)
    @NotNull
    private TypeTransaction typeTransaction;
    @Column(
            name = "amount",
            nullable = false
    )
    @Min(value = 0, message = "no puede transaccionar menos de $0")
    @NotNull
    private Double amount;//todo: esto es valor
    @Column(
            name = "balance",
            nullable = false
    )

    @NotNull
    private Double balance;//todo:esto es saldo
    @Column(name = "account_ref")

    private Long idAccountReference;

}
