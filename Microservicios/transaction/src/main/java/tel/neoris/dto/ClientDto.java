package tel.neoris.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tel.neoris.enums.GenderUtils;
import tel.neoris.enums.Status;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {
    @JsonProperty("idClient")
    private Long idClient;
    @JsonProperty("name")
    private String name;
    @JsonProperty("gender")
    private GenderUtils gender;
    @JsonProperty("age")
    private Integer age;
    @JsonProperty("identification")
    private String identification;
    @JsonProperty("address")
    private String address;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("password")
    private String password;
    @JsonProperty("status")
    private Status clientStatus;
    @JsonProperty("account")
    private List<BankAccountDto> bankAccountDto;



}
