package tel.neoris.service;



import tel.neoris.dto.ClientDto;
import tel.neoris.dto.TransactionDto;
import tel.neoris.model.BankAccount;

import java.util.List;
import java.util.Optional;

public interface BankAccountServicesInterface {
    List<?> getAllAccounts();
    String deletAccountById(Long id);
    Optional<BankAccount> findOneAccountById(Long id);
    BankAccount createAccount(BankAccount bankAccount);

    String cargoCuenta(Double cargo,Long id) throws Exception;

    List<?> selectTransaction();
    String deletByIdTransaction(Long id);

    TransactionDto inserTransaction(TransactionDto transactionDto);
    TransactionDto findOneTransaction(Long id);
    ClientDto findOneClient(Long id);

    List<BankAccount> findClientAccounts(Long id);





}
