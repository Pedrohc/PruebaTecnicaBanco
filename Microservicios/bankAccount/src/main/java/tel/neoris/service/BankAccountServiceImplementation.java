package tel.neoris.service;



import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tel.neoris.chainofresponsability.HandlerImplementation;
import tel.neoris.dto.ClientDto;
import tel.neoris.dto.TransactionDto;
import tel.neoris.exceptions.accountnotfound.AccountNotFound;
import tel.neoris.feing.ClientFeing;
import tel.neoris.feing.TransactionFeing;
import tel.neoris.model.BankAccount;
import tel.neoris.repository.BankAccountRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BankAccountServiceImplementation implements BankAccountServicesInterface {

    private BankAccountRepository bankAccountRepository;
    private TransactionFeing transactionFeing;
    private HandlerImplementation handlerImplementation;
    private ClientFeing clientFeing;

    @Override
    public List<?> getAllAccounts() {
        return bankAccountRepository.findAll();
    }

    @Override
    public String deletAccountById(Long id) {


        //Todo: falta ver si es realmente util hacer algo con esto
        String res = bankAccountRepository.findById(id).toString();
        bankAccountRepository.deleteById(id);
        return res;
    }

    @Override
    public Optional<BankAccount> findOneAccountById(Long id) {
        //todo: hacer una exception para devolver algo en caso de no encontrarlo
        return bankAccountRepository.findById(id);
    }

    @Override
    public BankAccount createAccount(BankAccount bankAccount) {
        return bankAccountRepository.save(bankAccount);
    }

    // TODO: 11/09/2023 esto hay que revisar que funcione el patron de chain of responsability


    @Override
    public String cargoCuenta(Double amount, Long id)  {
        if (bankAccountRepository.findById(id).isPresent()) {
            return handlerImplementation.handleCharge(amount, bankAccountRepository.findById(id).get());
        }
        throw new AccountNotFound("Cuenta no encontrada");


    }


    @Override
    public List<?> selectTransaction() {
        return transactionFeing.findAllTransaction();
    }

    @Override
    public String deletByIdTransaction(Long id) {
        //todo: revisa que hace este metodo para ver si funciona
        return transactionFeing.deleteOne(id).toString();
    }

    @Override
    public TransactionDto inserTransaction(TransactionDto transactionDto) {
        return transactionFeing.insert(transactionDto);
    }

    @Override
    public TransactionDto findOneTransaction(Long id) {
        return transactionFeing.findOneTransaction(id);
    }
    public ClientDto findOneClient(Long id) {
        return clientFeing.findOneClient(id);
    }

    @Override
    public List<BankAccount> findClientAccounts(Long id) {
        return bankAccountRepository.findBankAccountByIdClientReferenceEquals(id);
    }


}
