package tel.neoris.chainofresponsability;


import tel.neoris.model.BankAccount;

public interface Handler {
    String handleAccount(Long id, Double amount);
    boolean handleType(BankAccount account);
}
