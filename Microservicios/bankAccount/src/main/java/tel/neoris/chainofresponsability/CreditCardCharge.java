package tel.neoris.chainofresponsability;



import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tel.neoris.dto.BalanceReport;
import tel.neoris.dto.ClientDto;
import tel.neoris.dto.TransactionDto;
import tel.neoris.enums.TypeAccount;
import tel.neoris.enums.TypeTransaction;
import tel.neoris.exceptions.accountnotfound.AccountNotFound;
import tel.neoris.feing.ClientFeing;
import tel.neoris.feing.TransactionFeing;
import tel.neoris.model.BankAccount;
import tel.neoris.repository.BankAccountRepository;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreditCardCharge implements Handler {


    private BankAccountRepository bankAccountRepository;
    private ClientFeing clientFeing;
    private TransactionFeing transactionFeing;
   // private BankAccountServiceImplementation serviceImplementation;


    @Override
    public boolean handleType(BankAccount account) {
        return account.getTypeAccount() == TypeAccount.CREDITO;
    }


    @Override
    public String handleAccount(Long idCreditAccount, Double amount){
        if (bankAccountRepository.findById(idCreditAccount).isPresent()){
            BankAccount account = bankAccountRepository.findById(idCreditAccount).get();
            Double newBalance = account.getInitialBalance();
            //if (newBalance != 0) { // TODO: 11/09/2023 esta validacion no importa porque es credito
            //  if (amount < newBalance) { // TODO: 11/09/2023 esto tampoco porque no hay limite de credito aun
            newBalance += cargoCreditoNegativo(amount);
            account.setInitialBalance(newBalance);
            bankAccountRepository.save(account);// TODO: 19/09/2023 esto segun yo arregla el problema
            // bankAccountRepository.save(bankAccountRepository.findById(account.getIdBankAccount()).orElseGet(null));

            TransactionDto transactionDto = new TransactionDto( Timestamp.valueOf(LocalDateTime.now()),String.valueOf(LocalDate.now()), TypeTransaction.PAGO, amount, account.getInitialBalance(), account.getIdBankAccount());

            transactionFeing.insert(transactionDto);
           // serviceImplementation.inserTransaction(transactionDto);
            // TODO: 10/09/2023 Aqui vamos a obtener el nombre del cliente
            ClientDto clientDto = clientFeing.findOneClient(account.getIdClientReference());
            ;
            BalanceReport balanceReport = new BalanceReport(clientDto.getName(), newBalance, amount, Timestamp.valueOf(LocalDateTime.now()));
            return balanceReport.toString();
            // return clientDto.getName()+""+newBalance.toString()+""+amount.toString()+""+LocalDate.now().toString();
            //   } else {
            //      throw new Exception("No tienes suficiente saldo para realizar esta operación");
            //     }
            // } else {
            //      throw new Exception("No hay fondos suficientes en la cuenta");
            //  }

        }
        throw new AccountNotFound("No se encontro la cuenta asociada comuniquese con servicio al cliente");


    }

    private double cargoCreditoNegativo(double amount) {
        if (amount > 0) {
            amount = -amount;
            return amount;
        }
        return amount;
    }


}
