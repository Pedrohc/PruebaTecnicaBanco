package tel.neoris.chainofresponsability;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tel.neoris.exceptions.accountnotfound.AccountNotFound;
import tel.neoris.model.BankAccount;

import java.util.List;

@Component
@AllArgsConstructor
public class HandlerImplementation {
   private List<Handler> handlers;

    public String handleCharge(double amount, BankAccount account) {
        for (Handler handler:handlers){
            if (handler.handleType(account)){
                return handler.handleAccount(account.getIdBankAccount(),amount);
            }

        }throw new AccountNotFound("cuenta no encontrada");
    }

}
