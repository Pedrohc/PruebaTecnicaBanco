package tel.neoris.chainofresponsability;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import tel.neoris.dto.BalanceReport;
import tel.neoris.dto.ClientDto;
import tel.neoris.dto.TransactionDto;
import tel.neoris.enums.TypeAccount;
import tel.neoris.enums.TypeTransaction;
import tel.neoris.exceptions.accountnotfound.AccountNotFound;
import tel.neoris.exceptions.fondosinsuficientes.InsufficientFundsException;
import tel.neoris.feing.ClientFeing;
import tel.neoris.feing.TransactionFeing;
import tel.neoris.model.BankAccount;
import tel.neoris.repository.BankAccountRepository;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DebitCardCharge implements Handler {



    private BankAccountRepository bankAccountRepository;
    private ClientFeing clientFeing;
    private TransactionFeing transactionFeing;


    // TODO: 19/09/2023 Aeste metodo le pasas el id de la cuenta
    @Override
    public String handleAccount(Long idBankAccount, Double amount){
        if (bankAccountRepository.findById(idBankAccount).isPresent()){
            BankAccount account= bankAccountRepository.findById(idBankAccount).get();

            Double newBalance = account.getInitialBalance();
            if (newBalance > 0 && newBalance> amount) {
                newBalance -= amount;
                account.setInitialBalance(newBalance);
                TransactionDto transactionDto = new TransactionDto(0L, Timestamp.valueOf(LocalDateTime.now()),String.valueOf(LocalDate.now()),
                        TypeTransaction.PAGO, amount, account.getInitialBalance(),account.getIdBankAccount());

                transactionFeing.insert(transactionDto);
                bankAccountRepository.save(account);
                ClientDto clientDto = clientFeing.findOneClient(account.getIdClientReference());
                BalanceReport balanceReport = new BalanceReport(clientDto.getName(), newBalance, amount,Timestamp.valueOf(LocalDateTime.now()));
                return balanceReport.toString();

            } else {
                throw new InsufficientFundsException("Fondos insuficientes");
            }
        }
        throw new AccountNotFound("No se encontro la cuenta asociada comuniquese con servicio al cliente");



    }

    @Override
    public boolean handleType(BankAccount account) {
        return account.getTypeAccount().equals(TypeAccount.NOMINA);
    }


}

