package tel.neoris.abstractfactory;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tel.neoris.dto.BankAccountDto;
import tel.neoris.model.BankAccount;

@Component
@Qualifier("bankAccountDtoFactory")
public class BankAccountDtoFactory  implements DtoFactory {
    @Override
    public BankAccountDto createDto(Object entity) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, BankAccountDto.class);
    }

    @Override
    public BankAccount createEntity(Object entity) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, BankAccount.class);
    }
}
