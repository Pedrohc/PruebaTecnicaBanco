package tel.neoris.abstractfactory;



import tel.neoris.dto.BankAccountDto;
import tel.neoris.model.BankAccount;

public interface DtoFactory {
    BankAccountDto createDto(Object entity);
    BankAccount createEntity(Object entity);
}
