package tel.neoris.controller;


import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tel.neoris.abstractfactory.BankAccountDtoFactory;
import tel.neoris.dto.BankAccountDto;
import tel.neoris.dto.TransactionDto;
import tel.neoris.exceptions.ApiRequestException;
import tel.neoris.model.BankAccount;
import tel.neoris.service.BankAccountServiceImplementation;

import java.util.List;

@RestController
@RequestMapping
@AllArgsConstructor
public class BankAccountController {
    private BankAccountServiceImplementation serviceImp;
    private BankAccountDtoFactory bankAccountDtoFactory;



    // TODO: 11/10/2023 FALTA REVISAR QUE MANDE TODAS LAS EXCEPTIONS DE MANERA CORRECTA

    @GetMapping(value = "/select_accounts")
    public ResponseEntity<List<?>> findAll() {
        List<?> list1 = serviceImp.getAllAccounts();
        List<?> list = list1.stream()
                .map(bankAccountDtoFactory::createDto)
                .toList();
        return ResponseEntity.ok(list);
    }

    @PostMapping(value = "/insert_account")
    public ResponseEntity<BankAccountDto> insert(@Valid @RequestBody BankAccountDto bankAccountDto) {
        return ResponseEntity.ok(bankAccountDtoFactory.createDto(serviceImp.createAccount(bankAccountDtoFactory.createEntity(bankAccountDto))));


    }


    @PutMapping(value = "/update_account/")
    public ResponseEntity<BankAccountDto> update(@Valid @RequestBody BankAccount bankAccount) {

        if (serviceImp.findOneAccountById(bankAccount.getIdBankAccount()).isEmpty()) {
            throw new ApiRequestException("Error al intentar actualizar la cuenta, Cuenta no encontrada");
        }
        BankAccount res = serviceImp.createAccount(bankAccount);
        return ResponseEntity.ok().body(bankAccountDtoFactory.createDto(res));


    }

    @GetMapping(value = "/findOneAccount/{id}")
    public ResponseEntity<BankAccountDto> findOne(@PathVariable Long id) {
        return ResponseEntity.ok(serviceImp.findOneAccountById(id)
                        .map(bankAccountDtoFactory::createDto)
                .orElseThrow(() -> new ApiRequestException("No se encontro la cuenta")));
    }
    @GetMapping(value = "/findClientAccounts/{id}")
    public ResponseEntity<List<BankAccountDto>> findOneAccounts (@PathVariable Long id) {
        return ResponseEntity.ok(serviceImp.findClientAccounts(id)
                .stream()
                .map(bankAccountDtoFactory::createDto)
                .toList());
    }

    @DeleteMapping(value = "/delete_account/{id}")
    public ResponseEntity<String> deleteOne(@PathVariable Long id) {
        if (serviceImp.findOneAccountById(id).isPresent()) {
            String res = serviceImp.findOneAccountById(id).get().getIdBankAccount().toString();
            serviceImp.deletAccountById(id);
            return ResponseEntity.ok().body("Eliminaste la cuenta: " + res +
                    " ahora el banco te acaba de ascender al puesto de cliente...Bienvenido al desempleo!");
        }
        throw new ApiRequestException("no se encontro la cuenta a eliminar");

    }
    @GetMapping(value = "/selectTransactions")
    public ResponseEntity<?> findAllFeing() {
        List<TransactionDto> list = (List<TransactionDto>) serviceImp.selectTransaction();
        return ResponseEntity.ok(list);
    }

    @PostMapping(value = "/insertTransaction")
    public ResponseEntity<?> insertClient(@RequestBody TransactionDto transactionDto) {
        return ResponseEntity.ok( serviceImp.inserTransaction(transactionDto));
    }

    @PutMapping(value = "/updateTransaction")
    public ResponseEntity<?> updateClient(@RequestBody TransactionDto transactionDto) {
        return ResponseEntity.ok().body(serviceImp.inserTransaction(transactionDto));
    }


    @DeleteMapping(value = "/deleteTransaction/{id}")
    public ResponseEntity<?> deleteOneClient(@PathVariable Long id) {
        if (serviceImp.findOneAccountById(id).isPresent()){
            serviceImp.deletAccountById(id);
            return ResponseEntity.ok().body(serviceImp.deletByIdTransaction(id));
        }
        throw new ApiRequestException("no se encontro la transaccion a borrar");

    }
    @PutMapping(value = "cobro/{charge}/{id}")
    public ResponseEntity<String> charge(@PathVariable Double charge, @PathVariable Long id) {
        return ResponseEntity.ok(serviceImp.cargoCuenta(charge,id));
    }

}
