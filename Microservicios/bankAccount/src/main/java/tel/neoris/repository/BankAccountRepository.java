package tel.neoris.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tel.neoris.model.BankAccount;

import java.util.List;


public interface BankAccountRepository extends JpaRepository<BankAccount,Long> {

    List<BankAccount> findBankAccountByIdClientReferenceEquals(Long id);
}
