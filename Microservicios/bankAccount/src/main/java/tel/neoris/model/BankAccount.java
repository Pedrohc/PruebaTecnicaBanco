package tel.neoris.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tel.neoris.enums.Status;
import tel.neoris.enums.TypeAccount;
import tel.neoris.enumvalidator.EnumNamePattern;

@Entity(name = "BankAccount")
@Table(name = "bank_account")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "bank_account_id",
            nullable = false,
            updatable = false
    )
    private Long idBankAccount;

    @Column(
            name = "type_account",
            nullable = false
    )
    @Enumerated(EnumType.STRING)
    @NotNull
    @EnumNamePattern(regexp = "NOMINA|AHORRO|RETIRO|INVERSION|CREDITO")
    private TypeAccount typeAccount;

    @Column(
            name = "initial_Balance",
            nullable = false
    )

    private Double initialBalance;

   @Column(
           name = "status_account",
           nullable = false
   )

   @Enumerated(EnumType.STRING)
   @NotNull
   @EnumNamePattern(regexp = "ACTIVO|INACTIVO|DEUDOR|BURO")
   private Status statusAccount;



    @Column(
            name = "client_id_ref",
            nullable = false
    )
    private Long idClientReference;

    public BankAccount(TypeAccount typeAccount, Double initialBalance, Status statusAccount, Long idClientReference) {
        this.typeAccount = typeAccount;
        this.initialBalance = initialBalance;
        this.statusAccount = statusAccount;
        this.idClientReference = idClientReference;
    }

    public BankAccount(TypeAccount typeAccount, Double initialBalance, Status statusAccount) {
        this.typeAccount = typeAccount;
        this.initialBalance = initialBalance;
        this.statusAccount = statusAccount;
    }
}
