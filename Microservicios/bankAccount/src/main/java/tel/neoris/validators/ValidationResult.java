package tel.neoris.validators;

public enum ValidationResult {
    INVALID_ACCOUNT_TYPE, SUCCESS, TYPE_ACCOUNT_MISSING
}
