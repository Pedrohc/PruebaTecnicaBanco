package tel.neoris.validators;



import org.springframework.stereotype.Service;
import tel.neoris.enums.Status;
import tel.neoris.enums.TypeAccount;
import tel.neoris.model.BankAccount;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Function;


@Service
public interface AccountValidator extends Function<BankAccount,ValidationResult> {

    static AccountValidator isTypeAccountValid() {

        Set<TypeAccount> validTypes = EnumSet.allOf(TypeAccount.class);
        return account -> {
            if (account.getTypeAccount() == null) {
                return ValidationResult.TYPE_ACCOUNT_MISSING;
            }
           return validTypes
                   .contains(account.getTypeAccount()) ? ValidationResult.SUCCESS:ValidationResult.INVALID_ACCOUNT_TYPE;
        };
    }

    static AccountValidator isStatusValid() {
        Set<Status> validStatus = EnumSet.allOf(Status.class);

        return account -> {
            if (account.getTypeAccount() == null) {
                return ValidationResult.TYPE_ACCOUNT_MISSING;
            }
            return validStatus
                    .contains(account.getStatusAccount()) ? ValidationResult.SUCCESS:ValidationResult.INVALID_ACCOUNT_TYPE;
        };
    }

    default AccountValidator and(AccountValidator validator){
        return account -> {
            ValidationResult result = this.apply(account);
            return  result.equals(ValidationResult.SUCCESS)? validator.apply(account):result;
        };
    }

}
