package tel.neoris.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tel.neoris.enums.TypeTransaction;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class TransactionDto {

    private Long idTransaction;

    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss" // Ajusta el patrón para incluir fecha y hora
    )
    private Timestamp createdAt ;


    private String date ;

    private TypeTransaction typeTransaction;

    private Double amount;

    private Double balance;

    private Long idAccountReference;

    public TransactionDto(Timestamp createdAt, TypeTransaction typeTransaction, Double amount, Double balance, Long idAccountReference) {
        this.createdAt = createdAt;
        this.typeTransaction = typeTransaction;
        this.amount = amount;
        this.balance = balance;
        this.idAccountReference = idAccountReference;
    }

    public TransactionDto(Timestamp createdAt, String date, TypeTransaction typeTransaction, Double amount, Double balance, Long idAccountReference) {
        this.createdAt = createdAt;
        this.date = date;
        this.typeTransaction = typeTransaction;
        this.amount = amount;
        this.balance = balance;
        this.idAccountReference = idAccountReference;
    }
}
