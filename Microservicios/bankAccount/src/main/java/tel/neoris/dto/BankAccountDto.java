package tel.neoris.dto;



import lombok.Data;
import lombok.NoArgsConstructor;
import tel.neoris.enums.Status;
import tel.neoris.enums.TypeAccount;

@Data
@NoArgsConstructor
public class BankAccountDto {

    private Long idBankAccount;
    private TypeAccount typeAccount;
    private Double initialBalance;
    private Status statusAccount;
    private TransactionDto transactionDto;
    private Long idClientReference;

    public BankAccountDto(Long idBankAccount, TypeAccount typeAccount, Double initialBalance, Status statusAccount) {
        this.idBankAccount = idBankAccount;
        this.typeAccount = typeAccount;
        this.initialBalance = initialBalance;
        this.statusAccount = statusAccount;
    }


    public BankAccountDto(Long idBankAccount, TypeAccount typeAccount, Double initialBalance, Status statusAccount, TransactionDto transactionDto, Long idClientReference) {
        this.idBankAccount = idBankAccount;
        this.typeAccount = typeAccount;
        this.initialBalance = initialBalance;
        this.statusAccount = statusAccount;
        this.transactionDto = transactionDto;
        this.idClientReference = idClientReference;
    }
}
