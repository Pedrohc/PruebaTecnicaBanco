package tel.neoris;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import tel.neoris.enums.Status;
import tel.neoris.enums.TypeAccount;
import tel.neoris.model.BankAccount;
import tel.neoris.repository.BankAccountRepository;

@SpringBootApplication
@EnableFeignClients
public class BankAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankAccountApplication.class, args);
	}
	@Bean
	CommandLineRunner commandLineRunner(BankAccountRepository bankAccountRepository) {
		return args -> {
			extracted(bankAccountRepository);
		};
	}

	private static void extracted(BankAccountRepository bankAccountRepository) {


		for (int i = 0; i <= 20; i++) {
			if (i%2==0){
				bankAccountRepository.save(new BankAccount(TypeAccount.CREDITO,i*-3000.00, Status.ACTIVO, (long) i));
			}else if (i%3==0){
				bankAccountRepository.save(new BankAccount(TypeAccount.NOMINA,i*7500.00,Status.ACTIVO,(long) i));
			}
			bankAccountRepository.save(new BankAccount(TypeAccount.INVERSION,i*50000.00,Status.ACTIVO,(long) i));



		}
	}


}
