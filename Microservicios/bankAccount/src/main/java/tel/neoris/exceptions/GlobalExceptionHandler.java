package tel.neoris.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import tel.neoris.exceptions.accountnotfound.AccountNotFound;
import tel.neoris.exceptions.accountnotfound.AccountNotFoundResponse;
import tel.neoris.exceptions.fondosinsuficientes.InsufficientFundsException;
import tel.neoris.exceptions.fondosinsuficientes.InsufficientFundsExceptionResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestControllerAdvice
public class GlobalExceptionHandler  extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException exception) {
        ApiException apiException = new ApiException(
                exception.getMessage(),
                exception.getErrors(),
                // Agrega el valor del campo afectado
                HttpStatus.BAD_REQUEST
        );
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected  ResponseEntity<Object>handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders httpHeaders, HttpStatusCode status, WebRequest request){

        Map<String,String> errors = new HashMap<>();
        List<ObjectError> errorList= ex.getBindingResult().getAllErrors();
        errorList.forEach(error->{
            String fieldName= ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName,message);
        });
        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);

    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({AccountNotFound.class})
    public ResponseEntity<Object> handleAccountNotFoundException(AccountNotFound exception) {
        AccountNotFoundResponse accountNotFound = new AccountNotFoundResponse(
                exception.getMessage(),
                HttpStatus.BAD_REQUEST
        );
        return new ResponseEntity<>(accountNotFound, HttpStatus.BAD_REQUEST);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InsufficientFundsException.class})
    public ResponseEntity<Object> handleNotEnoughFundsException(InsufficientFundsException exception) {
        InsufficientFundsExceptionResponse fundsExceptionResponse = new InsufficientFundsExceptionResponse(
                exception.getMessage(),
                HttpStatus.BAD_REQUEST
        );
        return new ResponseEntity<>(fundsExceptionResponse, HttpStatus.BAD_REQUEST);
    }



}
