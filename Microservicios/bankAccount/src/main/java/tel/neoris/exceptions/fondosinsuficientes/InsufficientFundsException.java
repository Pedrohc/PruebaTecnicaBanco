package tel.neoris.exceptions.fondosinsuficientes;

public class InsufficientFundsException extends RuntimeException{
    public InsufficientFundsException(String message) {
        super(message);
    }
}
