package tel.neoris.exceptions.accountnotfound;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountNotFoundResponse {

   private  String message;
   private HttpStatus httpStatus;

}
