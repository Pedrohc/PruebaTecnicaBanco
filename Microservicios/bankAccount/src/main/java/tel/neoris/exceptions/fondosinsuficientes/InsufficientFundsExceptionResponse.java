package tel.neoris.exceptions.fondosinsuficientes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InsufficientFundsExceptionResponse {

    private String message;
    private HttpStatus httpStatus;
}
