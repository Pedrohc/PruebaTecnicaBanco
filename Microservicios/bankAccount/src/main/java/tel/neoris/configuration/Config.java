package tel.neoris.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tel.neoris.validators.AccountValidator;

@Configuration
public class Config {


    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        return objectMapper ;
    }
    @Bean
    public AccountValidator typeValidator() {
        return AccountValidator.isTypeAccountValid();
    }

    @Bean
    public AccountValidator statusValidator() {
        return AccountValidator.isStatusValid();
    }

}
