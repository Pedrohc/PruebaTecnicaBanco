package tel.neoris.feing;



import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import tel.neoris.dto.ClientDto;

@FeignClient(name = "CLIENT-SERVICE")
public interface ClientFeing {
    @GetMapping(value = "findOneClient/{id}")
    ClientDto findOneClient(@PathVariable("id") Long id);

}
