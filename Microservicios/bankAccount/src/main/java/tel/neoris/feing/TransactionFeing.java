package tel.neoris.feing;



import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import tel.neoris.dto.TransactionDto;

import java.util.List;

@FeignClient(name = "TRANSACTION-SERVICE")
public interface TransactionFeing {

    @GetMapping(value = "/selectTransactions")
    List<TransactionDto> findAllTransaction();
    @PostMapping(value = "/insertTransaction")
    TransactionDto  insert(@RequestBody TransactionDto transactionDto);
    @PutMapping(value = "/updateTransaction")
    TransactionDto  update(@RequestBody TransactionDto transactionDto);
    @DeleteMapping(value = "/deleteTransaction/{id}")
    Long deleteOne(@PathVariable("id") Long id) ;
    @GetMapping(value = "/findOneTransaction/{id}")
    TransactionDto  findOneTransaction(@PathVariable("id") Long id);

}
