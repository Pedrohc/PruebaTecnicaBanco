package tel.neoris;

import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import tel.neoris.enums.GenderUtils;
import tel.neoris.enums.Status;
import tel.neoris.model.Client;
import tel.neoris.repository.ClientRepository;

import java.util.Random;

@SpringBootApplication
@EnableFeignClients
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}
	@Bean
	CommandLineRunner commandLineRunner(ClientRepository clientRepository) {
		return args -> {
			extracted(clientRepository);
		};
	}

	private static void extracted(ClientRepository clientRepository) {
		Faker faker = new Faker();
		Random random = new Random();
		int age = 0;
		String identification = "";
		String address = "";
		String phone = "";
		String password = "";
		for (int i = 0; i <= 20; i++) {
			String firstName = faker.name().firstName() + faker.name().lastName();
			age = random.nextInt(83) + 18;
			identification = faker.ancient().primordial();
			address = faker.address().fullAddress();
			phone = faker.phoneNumber().phoneNumber();
			password = faker.artist().name();


			clientRepository.save(new Client(firstName, GenderUtils.OTHER, age, identification, address, phone, password, Status.ACTIVO));
		}
	}
}
