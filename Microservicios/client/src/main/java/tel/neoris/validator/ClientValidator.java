package tel.neoris.validator;


import org.springframework.stereotype.Component;
import tel.neoris.enums.GenderUtils;
import tel.neoris.enums.Status;
import tel.neoris.enums.ValidationResult;
import tel.neoris.model.Client;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Function;


@Component
public interface ClientValidator extends Function<Client, ValidationResult> {

    static ClientValidator isPasswordValid() {

        return client -> {
            if (client.getPassword() == null) {
                return ValidationResult.PASSWORD_NOT_FOUND;
            }
            return client.getPassword().matches("^[a-zA-Z0-9._%+-]{8,16}$") ?
                    ValidationResult.SUCCESS : ValidationResult.INVALID_PASSWORD;
        };
    }

    static ClientValidator isNameValid() {
        return client -> {
            if (client.getName() == null) {
                return ValidationResult.NAME_NOT_FOUND;
            }
            return client.getName().matches("^[a-zA-Z ']+$") ?
                    ValidationResult.SUCCESS : ValidationResult.INVALID_NAME;
        };

    }

    static ClientValidator isPhoneNumberValid() {
        return client -> {
            if (client.getPhoneNumber()==null){
                return ValidationResult.PHONE_NUMBER_NOT_FOUND;
            }
            return client.getPhoneNumber().toString().matches("^[0-9]{10,11}$") ?
                    ValidationResult.SUCCESS : ValidationResult.INVALID_PHONE_NUMBER;
        };

    }

    static ClientValidator isGenderValid() {
        Set<GenderUtils> validGenders = EnumSet.allOf(GenderUtils.class);//cambie esto
        return client -> {
            if (client.getGender()==null){
                return ValidationResult.GENDER_NOT_FOUND;
            }
            return validGenders.
                    contains(client.getGender()) ? ValidationResult.SUCCESS : ValidationResult.INVALID_GENDER;
        };

    }

    static ClientValidator isAgeValid() {

        return client -> {
            if (client.getAge() == null) {
                return ValidationResult.AGE_NOT_FOUND;
            }
            return client.getAge() > 18 ? ValidationResult.SUCCESS : ValidationResult.INVALID_AGE;
        };
    }

    static ClientValidator isIdentificationValid() {
        //SOLO ESTOY PREGUNTANDO SI ESTA PRESENTE NO TENGO IDENTIFICACIONES EN ENUM
        return client -> {
            String identification = client.getIdentification();
            return identification != null && !identification.trim().isEmpty() ? ValidationResult.SUCCESS : ValidationResult.IDENTIFICATION_NOT_FOUND;
        };
    }

    static ClientValidator isAddressValid() {
        return client -> {
            String address = client.getAddress();
            return address != null && !address.trim().isEmpty() ? ValidationResult.SUCCESS : ValidationResult.ADDRESS_NOT_FOUND;
        };
    }


    static ClientValidator isStatusValid() {
        Set<Status> validStatus = EnumSet.of(Status.ACTIVO,
                Status.DEUDOR, Status.BURO, Status.INACTIVO);
        return client -> {
            if (client.getStatus()==null || !validStatus.contains(client.getStatus())){
                return ValidationResult.STATUS_NOT_FOUND;
            }
            return validStatus.contains(client.getStatus()) ? ValidationResult.SUCCESS : ValidationResult.INVALID_STATUS;
        };

    }

    default ClientValidator and(ClientValidator validator) {
        return client -> {
            ValidationResult result = this.apply(client);
            return result.equals(ValidationResult.SUCCESS) ? validator.apply(client) : result;
        };

    }
}
