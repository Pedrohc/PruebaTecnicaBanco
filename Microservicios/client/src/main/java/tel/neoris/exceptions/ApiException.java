package tel.neoris.exceptions;


import org.springframework.http.HttpStatus;
import tel.neoris.enums.ValidationResult;

import java.util.List;

public class ApiException {
    private final String message;
    // Agrega el valor del campo
    private final List<ValidationResult> errors;
    private final HttpStatus httpStatus;

    public ApiException(String message, List<ValidationResult> errors, HttpStatus httpStatus) {
        this.message = message;
        this.errors = errors;
        this.httpStatus = httpStatus;
    }



    public String getMessage() {
        return message;
    }

    public List<ValidationResult> getErrors() {
        return errors;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
