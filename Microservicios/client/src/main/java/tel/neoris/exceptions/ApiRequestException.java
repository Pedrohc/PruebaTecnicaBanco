package tel.neoris.exceptions;


import tel.neoris.enums.ValidationResult;

import java.util.List;

public class ApiRequestException extends RuntimeException{

    private List<ValidationResult> errors;// Agrega el valor del campo

    public ApiRequestException(String message, List<ValidationResult> errors) {
        super(message);
        this.errors = errors;
    }

    public ApiRequestException(String message) {
        super(message);
    }

    public List<ValidationResult> getErrors() {
        return errors;
    }
}
