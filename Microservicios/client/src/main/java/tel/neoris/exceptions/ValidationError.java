package tel.neoris.exceptions;

import lombok.Getter;

@Getter
public class ValidationError {
    private final String fieldName;
    private final String errorMessage;

    public ValidationError(String fieldName, String errorMessage) {
        this.fieldName = fieldName;
        this.errorMessage = errorMessage;
    }

}
