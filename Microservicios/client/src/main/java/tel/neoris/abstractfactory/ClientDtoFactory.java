package tel.neoris.abstractfactory;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tel.neoris.dto.ClientDto;
import tel.neoris.model.Client;

@Component
@Qualifier("clientDtoFactory")
public class ClientDtoFactory implements DtoFactory {


    @Override
    public ClientDto createDto(Object entity) {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.convertValue(entity, ClientDto.class);
    }

    @Override
    public Client createEntity(Object entity) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, Client.class);
    }
}
