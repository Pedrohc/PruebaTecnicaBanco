package tel.neoris.abstractfactory;



import tel.neoris.dto.ClientDto;
import tel.neoris.model.Client;

public interface DtoFactory {
    ClientDto createDto(Object entity);
    Client createEntity(Object entity);
}
