package tel.neoris.enums;

public enum TypeTransaction {
    PAGO,
    RETIRO,
    TRANSFERENCIA,
    PAGOSERVICIOS,
}
