package tel.neoris.feing;



import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import tel.neoris.dto.BankAccountDto;

import java.util.List;

@FeignClient(name = "BANK-ACCOUNT-SERVICE")
public interface BankAccountFeing {

    @GetMapping(value = "/findClientAccounts/{id}")
    List<BankAccountDto> findAccountList(@PathVariable("id") Long id);
}
