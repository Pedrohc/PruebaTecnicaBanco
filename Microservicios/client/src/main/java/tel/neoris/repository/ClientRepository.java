package tel.neoris.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tel.neoris.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {
}
