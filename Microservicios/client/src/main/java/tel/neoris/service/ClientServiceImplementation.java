package tel.neoris.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tel.neoris.dto.BankAccountDto;

import tel.neoris.feing.BankAccountFeing;
import tel.neoris.model.Client;
import tel.neoris.repository.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientServiceImplementation implements ClientService{

    private ClientRepository clientRepository;
    private BankAccountFeing bankAccountFeing;
    @Override
    public List<?> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Optional<Client> getClientById(Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public Optional<Client> saveClient(Client client) {
        return Optional.of(clientRepository.save(client));
    }

    @Override
    public String deleteClient(Long id) {
        return clientRepository
                .findById(id)
                .map(client ->{
                    clientRepository.deleteById(id);
                    return client.toString();
                }).orElse("Cliente no encontrado");
    }
    @Override
    public List<BankAccountDto> findClientsAccounts(Long id) {
        return bankAccountFeing.findAccountList(id);
    }
    @Override
    public Optional<Client> findOneClientById(Long id) {
        return clientRepository.findById(id);
    }
}
