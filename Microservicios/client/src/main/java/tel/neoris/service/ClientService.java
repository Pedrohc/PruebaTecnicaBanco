package tel.neoris.service;



import tel.neoris.dto.BankAccountDto;
import tel.neoris.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {
    List<?> getAllClients();
    Optional<Client> getClientById(Long id);

    Optional<Client>  saveClient(Client client);

    String deleteClient(Long id);

    List<BankAccountDto> findClientsAccounts(Long id);

    Optional<Client>  findOneClientById(Long id);


}
