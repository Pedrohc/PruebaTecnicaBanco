package tel.neoris.configuration;

import org.springframework.context.annotation.Bean;
import tel.neoris.validator.ClientValidator;


@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean
    public ClientValidator passwordValidator() {
        return ClientValidator.isPasswordValid();
    }

    @Bean
    public ClientValidator nameValidator() {
        return ClientValidator.isNameValid();
    }
    @Bean
    public ClientValidator phoneNumberValidator() {
        return ClientValidator.isPhoneNumberValid();
    }
    @Bean
    public ClientValidator genderValidator() {
        return ClientValidator.isGenderValid();
    }
    @Bean
    public ClientValidator ageValidator() {
        return ClientValidator.isAgeValid();
    }
    @Bean
    public ClientValidator identificationValidator() {
        return ClientValidator.isIdentificationValid();
    }
    @Bean
    public ClientValidator addressValidator() {
        return ClientValidator.isAddressValid();
    }

    @Bean
    public ClientValidator statusValidator() {
        return ClientValidator.isStatusValid();
    }
}
