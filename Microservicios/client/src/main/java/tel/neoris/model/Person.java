package tel.neoris.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import tel.neoris.enums.GenderUtils;


@MappedSuperclass
@Data
public class Person {
    @NotEmpty(message = "El nombre no debe de estar vacio")
    @NotNull
    @JsonProperty("name")
    private String name;

    @NotNull(message = "no puede estar vacio")
    @Enumerated(value = EnumType.STRING)
    @JsonProperty("gender")
    @Column(columnDefinition = "varchar(50)")
    private GenderUtils gender;
    @Min(value = 18,message = "Debes ser mayor de edad para abrir una cuenta")
    @NotNull
    @JsonProperty("age")
    private Integer age;
    @NotEmpty(message = "La identification no debe de estar vacia")
    @NotNull
    @JsonProperty("identification")
    private String identification;
    @NotEmpty(message = "El address no debe de estar vacio")
    @NotNull
    @JsonProperty("address")
    private String address;
    @NotEmpty(message = "El phoneNumber no debe de estar vacio")
    @NotNull
    @JsonProperty("phoneNumber")
    private String phoneNumber;

    public Person() {
    }

    public Person(String name, GenderUtils gender, Integer age, String identification, String address, String phoneNumber) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.identification = identification;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }
}
