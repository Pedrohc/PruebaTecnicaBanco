package tel.neoris.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import tel.neoris.enums.GenderUtils;
import tel.neoris.enums.Status;


@Entity(name = "Client")
@Table(name = "client")
@AllArgsConstructor

@Data
public class Client extends Person{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client",unique = true,updatable = false)
    @JsonProperty("idClient")
    private Long id;
    @NotEmpty(message = "El password no puede venir vacio")
    @Column(name = "password",nullable = false)
    @JsonProperty("password")
    @NotEmpty
    private String password;

    @Column(
            name = "status",
            nullable = false,
            columnDefinition = "varchar(50)"
    )
    @Enumerated(value = EnumType.STRING)
    @JsonProperty("status")

    private Status status;

    public Client() {
    }


    public Client(String name, GenderUtils gender, Integer age, String identification, String address, String phoneNumber, String password, Status status) {
        super(name, gender, age, identification, address, phoneNumber);
        this.password = password;
        this.status = status;
    }
}
