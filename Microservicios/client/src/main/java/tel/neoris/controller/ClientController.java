package tel.neoris.controller;


import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tel.neoris.abstractfactory.DtoFactory;
import tel.neoris.dto.ClientDto;
import tel.neoris.enums.ValidationResult;
import tel.neoris.exceptions.ApiRequestException;
import tel.neoris.model.Client;
import tel.neoris.service.ClientServiceImplementation;
import tel.neoris.validator.ClientValidator;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping
@AllArgsConstructor
@Slf4j
public class ClientController {

    private DtoFactory clientDtoFactory;
    private ClientValidator passwordValidator;

    private ClientValidator nameValidator;

    private ClientValidator phoneNumberValidator;

    private ClientValidator genderValidator;

    private ClientValidator ageValidator;

    private ClientValidator identificationValidator;

    private ClientValidator addressValidator;

    private ClientValidator statusValidator;


    private ClientServiceImplementation serviceImplementation;

    @GetMapping(path = "/getAllClients")
    public List<?> getAllClients() {
        return serviceImplementation.getAllClients()
                .stream()
                .map(clientDtoFactory::createDto)
                .toList();
    }


    @PostMapping(path = "/createClient")
    public ResponseEntity<Client> createClient(@RequestBody Client client) {
        return validateClient(client);


    }

    @DeleteMapping(path = "/deleteClient/{id}")
    public ResponseEntity<String> deleteClient(@PathVariable Long id) {

        //Metodo listo
        if (serviceImplementation.findOneClientById(id).isEmpty()) {
            throw new ApiRequestException("Cliente no encontrado");
        }
        serviceImplementation.deleteClient(id);
        // serviceImplementation.deletAccountById(id); esto es para cuando eliminas tambien la cuenta esto es comunicacion entre microservicios

        return ResponseEntity.ok().body("Eliminaste el id: " + id);

    }

    @PutMapping(path = "/updateClient")
    public ResponseEntity<Client> updateClient(@Valid @RequestBody Client client) {
        return validateClient(client);

    }
    @GetMapping(value = "/findOneClient/{id}")
    public ResponseEntity<ClientDto> findOneClient(@PathVariable Long id) {
        // TODO: 25/10/2023 se tendra que revisar si funciona bien la exception
        ClientDto dto = serviceImplementation.findOneClientById(id).
                map(clientDtoFactory::createDto)
                .orElseThrow(() -> new ApiRequestException("Cliente no encontrado"));
        return ResponseEntity.ok(dto);
    }
    @GetMapping(value = "/allClientsAccounts")
    public ResponseEntity<?> findAllBankAccounts() {
        List<ClientDto> list = serviceImplementation.getAllClients().stream()
                .map(clientDtoFactory::createDto)
                .toList();
        list.forEach(clientDto -> {
           clientDto.setBankAccountDto(serviceImplementation.findClientsAccounts(clientDto.getIdClient()));

        });
        return ResponseEntity.ok(list);
    }


    private ResponseEntity<Client> validateClient(@RequestBody @Valid Client client) {
        List<ValidationResult> validationResults = Arrays.asList(
                genderValidator.apply(client),
                nameValidator.apply(client),
                phoneNumberValidator.apply(client),
                ageValidator.apply(client),
                identificationValidator.apply(client),
                addressValidator.apply(client),
                statusValidator.apply(client),
                passwordValidator.apply(client)
        );
        if (validationResults.stream().allMatch(ValidationResult.SUCCESS::equals)) {
            serviceImplementation.saveClient(client);
            return ResponseEntity.ok().body(client);
        } else {
            List<ValidationResult> errorDetails = validationResults.stream()
                    .filter(result -> !ValidationResult.SUCCESS.equals(result))
                    .toList();

            throw new ApiRequestException("Datos del Cliente no son válidos", errorDetails);
        }
    }
}



